package com.bigdogxh.learnspringboot.aware;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.bigdogxh.learnspringboot.aware")
public class AwareConfig {
}
