package com.bigdogxh.learnspringboot.aware;

import lombok.Data;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.Charset;


@Service
@Data
public class AweraService implements BeanNameAware, ResourceLoaderAware {
    private String beanName;
    private ResourceLoader resourceLoader;

   public void outputResult(){
       System.out.println("Bean的名称：" + beanName);

       Resource resource = resourceLoader.getResource("classpath:aware.txt");
       try {
           System.out.println("ResourceLoader加载的文件内容为：" + IOUtils.toString(resource.getInputStream(), Charset.defaultCharset()));
       } catch (IOException e) {
           e.printStackTrace();
       }
   }
}
