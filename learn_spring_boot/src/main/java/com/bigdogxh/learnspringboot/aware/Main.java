package com.bigdogxh.learnspringboot.aware;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context  = new AnnotationConfigApplicationContext(AwareConfig.class);
        AweraService aweraService = context.getBean(AweraService.class);
        aweraService.outputResult();
        context.close();
    }
}
