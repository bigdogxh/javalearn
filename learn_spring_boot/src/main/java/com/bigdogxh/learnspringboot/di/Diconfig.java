package com.bigdogxh.learnspringboot.di;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.bigdogxh.learnspringboot")
public class Diconfig {
}
