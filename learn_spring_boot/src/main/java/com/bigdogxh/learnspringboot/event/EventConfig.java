package com.bigdogxh.learnspringboot.event;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.bigdogxh.learnspringboot.event")
public class EventConfig {

}
