package com.bigdogxh.learnspringboot.fortest;

import lombok.Data;

@Data
public class TestBean {

    private String content;

    public TestBean(String content){
        super();
        this.content = content;
    }
}
