package com.bigdogxh.learnspringboot.mvc.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloConfig {

    @RequestMapping("/index")
    public String hello(){
        return "index";
    }
}
