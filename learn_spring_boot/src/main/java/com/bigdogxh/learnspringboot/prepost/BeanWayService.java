package com.bigdogxh.learnspringboot.prepost;

public class BeanWayService {
    public void init(){
        System.out.println("@Bean-init-method");
    }
    public BeanWayService(){
        super();
        System.out.println("初始化");
    }
    public void destory(){
        System.out.println("@Bean-destory-method");
    }
}
