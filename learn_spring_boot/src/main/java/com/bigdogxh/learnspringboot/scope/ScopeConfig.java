package com.bigdogxh.learnspringboot.scope;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.bigdogxh.learnspringboot")
public class ScopeConfig {
}
