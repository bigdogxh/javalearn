package com.bigdogxh.learnspringboot.taskscheduler;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan("com.bigdogxh.learnspringboot.taskscheduler")
@EnableScheduling
public class TaskSchedulerConfig {

}
